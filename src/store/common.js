const state = {
  config: {
    APIURL: process.env.VUE_APP_APIURL,
  },
  dataLoading: {},
};
const getters = {
  getDataLoading: (state) => (id) => id && state.dataLoading.hasOwnProperty(id) && state.dataLoading[id] === true,
};
const mutations = {
  setLoading (state, dataLoading) {
    state.dataLoading = {...state.dataLoading, ...dataLoading}
  },
};
const actions = {
  setLoading ({commit}, data) {
    commit('setLoading', data)
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
