import Vue from 'vue'
import Router from 'vue-router'
import StarwarsPeople from './views/Starwars-people'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: StarwarsPeople,
      props: true,
      children: [
        {
          path: 'people/:peopleId',
          name: 'starwarsSingle',
          component: StarwarsPeople,
        },
        {
          path: '',
          name: 'starwarsList',
          component: StarwarsPeople,
        },
      ],
    }
  ]
})
