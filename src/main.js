import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbvue/lib/css/mdb.min.css'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios/dist/axios'
import VueAxios from 'vue-axios/dist/vue-axios.min'
import App from './App.vue'
import router from './router'
import commonStore from './store/common'

Vue.use(VueAxios, axios);
Vue.use(Vuex);
const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    commonStore,
  },
});

Vue.config.productionTip = false

import Notify from 'mdbvue/lib/components/Notify'
Vue.use(Notify)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
